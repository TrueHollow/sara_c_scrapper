const Sequelize = require('sequelize');
const config = require('../config');
const logger = require('../logger')('db/index.js');

const configDB = config.database;
if (configDB.logging === true) {
  configDB.logging = message => {
    logger.debug(message);
  };
}

const sequelize = new Sequelize(configDB);

const db = {
  sequelize,
  Sequelize,
};

db.Link = require('./models/Links')(db);

module.exports = db;
