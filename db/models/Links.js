const Sequelize = require('sequelize');

module.exports = ({ sequelize }) => {
  return sequelize.define('links', {
    link: { type: Sequelize.STRING, allowNull: false, unique: true },
    isStageFinished: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  });
};
