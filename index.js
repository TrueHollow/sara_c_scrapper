const logger = require('./logger')('index.js');
const LinkParser = require('./service/LinkParser');
const PersonsParser = require('./service/PersonsParser');
const IO = require('./service/IO');
const config = require('./config');

logger.info('Script is started');

const main = async () => {
  await IO.Init();
  if (!IO.canContinue) {
    logger.info('Clean start.');
    const linkParser = new LinkParser(config.Parser);
    await linkParser.saveLinksToDb();
  } else {
    logger.info('Restoring state.');
  }
  const personParser = new PersonsParser(config.Parser);
  await personParser.run();
};

main().then(() => {
  logger.info('Script is finished.');
});
