const fs = require('fs');
const path = require('path');
const stringify = require('csv-stringify');
const logger = require('../logger')('service/IO.js');
const db = require('../db');

const OUTPUT_DIRECTORY = path.resolve(__dirname, '../output');
const PERSONS_CSV_FILENAME = 'persons.csv';

const createOutputDirectory = async () => {
  return new Promise((resolve, reject) => {
    fs.mkdir(`${OUTPUT_DIRECTORY}`, { recursive: true }, err => {
      if (err) {
        return reject(err);
      }
      logger.debug('Output directory was created.');
      return resolve();
    });
  });
};

const saveStringToFile = (str, filename) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(filename, str, { flag: 'a' }, err => {
      if (err) {
        return reject(err);
      }
      return resolve();
    });
  });
};

const getCSVString = async arr => {
  return new Promise((resolve, reject) => {
    stringify(
      arr,
      {
        delimiter: ',',
      },
      (err, records) => {
        if (err) {
          return reject(err);
        }
        return resolve(records);
      }
    );
  });
};

let canContinue = false;

const checkCanContinue = async () => {
  const { Link, sequelize } = db;
  await sequelize.sync();
  return Link.findOne({ where: { isStageFinished: true } });
};

const deletePrevPersons = async () => {
  const fullPath = path.resolve(OUTPUT_DIRECTORY, PERSONS_CSV_FILENAME);
  logger.debug('Deleting old %s', PERSONS_CSV_FILENAME);
  return new Promise((resolve, reject) => {
    fs.unlink(fullPath, err => {
      if (err) {
        if (err.code === 'ENOENT') {
          logger.debug('Old %s not found. Skipping.', PERSONS_CSV_FILENAME);
          resolve();
        } else {
          reject(err);
        }
      }
      resolve();
    });
  });
};

class IO {
  static async Init() {
    await createOutputDirectory();
    if (!(await checkCanContinue())) {
      const { Link } = db;
      await Promise.all([
        deletePrevPersons(),
        Link.destroy({ where: {}, truncate: true }),
      ]);
    } else {
      canContinue = true;
    }
  }

  static get canContinue() {
    return canContinue;
  }

  static async SaveCodeAreaLinks(linkArrays) {
    const { Link } = db;
    logger.debug('Saving (%d) rows to database.', linkArrays.length);
    return Link.bulkCreate(
      linkArrays.map(link => {
        return { link, isStageFinished: true };
      })
    );
  }

  static async SavePersonsCSV(personsArr) {
    if (personsArr.length) {
      const personsCsvRaw = [];
      personsArr.forEach(person => {
        const { owners } = person;
        if (!Array.isArray(owners)) {
          return;
        }
        const phoneNumber = person.phone_number ? person.phone_number : null;
        const details = Array.isArray(person.details) ? person.details : [];
        const location = details.find(d => d.title === 'location');
        let city = null;
        let state = null;
        if (location) {
          const parts = location.value.split(',');
          if (parts.length !== 2) {
            state = location.value;
          } else {
            city = parts[0].trim();
            state = parts[1].trim();
          }
        }
        owners.forEach(owner => {
          if (owner.indexOf('Null ') === -1) {
            const name = owner.replace(/\*/g, '');
            personsCsvRaw.push([phoneNumber, name, city, state]);
          } else {
            logger.warn('Skipping (Null) %s', {
              phoneNumber,
              owner,
              city,
              state,
            });
          }
        });
      });
      const csvString = await getCSVString(personsCsvRaw);
      const fullPath = path.resolve(OUTPUT_DIRECTORY, PERSONS_CSV_FILENAME);
      await saveStringToFile(csvString, fullPath);
      logger.info(
        'Persons count (%d), line count (%d) was added to %s',
        personsArr.length,
        personsCsvRaw.length,
        PERSONS_CSV_FILENAME
      );
    }
  }
}

module.exports = IO;
