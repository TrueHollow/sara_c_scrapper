const cheerio = require('cheerio');
const Net = require('./Net');
const IO = require('./IO');
const logger = require('../logger')('service/Parser.js');
const db = require('../db');

const SPOKEO_HOST = 'https://www.spokeo.com';
const INITIAL_URL = `${SPOKEO_HOST}/reverse-address-search`;

class LinkParser {
  constructor(config) {
    this.config = config;
  }

  async getAddressDirectory() {
    const html = await Net.GetHtmlPage(INITIAL_URL);
    const $ = cheerio.load(html);
    const aTags = $('a.homepage_browse_item');
    const directories = [];
    aTags.each((i, aTag) => {
      const { href } = aTag.attribs;
      if (href) {
        if (href.lastIndexOf('/') === 0) {
          directories.push(href);
        }
      } else {
        logger.warn('No href for directory');
      }
    });
    return directories;
  }

  async parseAreaCode(url) {
    const areaCodeURL = `${SPOKEO_HOST}${url}`;
    const html = await Net.GetHtmlPage(areaCodeURL);
    const $ = cheerio.load(html);
    const divs = $('#phone_prefixes div');
    let codeAreas = [];
    divs.each((i, div) => {
      const reactBlock = div.attribs['data-react-class'];
      const props = div.attribs['data-react-props'];
      if (reactBlock === 'packs/v9/phone/containers/AreaCodeListing' && props) {
        const data = JSON.parse(props);
        codeAreas = data.areaCodes
          .map(areaCode => {
            return areaCode.href;
          })
          .filter(l => l);
      }
    });
    return codeAreas;
  }

  async parseAreas(directory) {
    const directoryURL = `${SPOKEO_HOST}${directory}`;
    const html = await Net.GetHtmlPage(directoryURL);
    const $ = cheerio.load(html);

    const aTags = $('.directory-link a');
    const links = [];

    aTags.each((i, aTag) => {
      const { href } = aTag.attribs;
      if (href && href.indexOf('-area-code') !== -1) {
        links.push(href);
      }
    });

    const { RequestsLimit } = this.config;
    let result = [];
    do {
      const linkIteration = links.splice(0, RequestsLimit);
      // eslint-disable-next-line no-await-in-loop
      const resultIteration = await Promise.all(
        linkIteration.map(link => this.parseAreaCode(link))
      );
      result = result.concat(...resultIteration);
    } while (links.length);

    return result;
  }

  async saveLinksToDb() {
    const directories = await this.getAddressDirectory();

    const links = new Set();
    // eslint-disable-next-line no-restricted-syntax
    for (const directory of directories) {
      // eslint-disable-next-line no-await-in-loop
      const partLinks = await this.parseAreas(directory);

      partLinks.forEach(link => {
        if (link) {
          links.add(link);
        } else {
          logger.warn('Unexpected link %s', link);
        }
      });
      logger.debug('Links count %d', links.size);
    }
    await IO.SaveCodeAreaLinks(Array.from(links));
  }
}

module.exports = LinkParser;
