const request = require('request');
const logger = require('../logger')('service/Net.js');
const config = require('../config');

let position = -1;
const getNewProxy = () => {
  const { ProxyPool } = config;
  if (!Array.isArray(ProxyPool) || ProxyPool.length === 0) {
    return;
  }
  position += 1;
  if (position >= ProxyPool.length) {
    position = 0;
  }
  const proxy = ProxyPool[position];
  process.env.http_proxy = proxy;
  process.env.https_proxy = proxy;
};

const HEADERS = {
  'User-Agent':
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
};

const HTTP_OK = 200;

const delay = async (timeout = 5000) => {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
};

const GetRequest = async url => {
  return new Promise((resolve, reject) => {
    getNewProxy();
    request(
      {
        url,
        method: 'GET',
        headers: HEADERS,
        gzip: true,
        timeout: 20000,
      },
      async (err, incomingMessage, html) => {
        if (err) {
          return reject(err);
        }
        if (incomingMessage.statusCode !== HTTP_OK) {
          return reject(
            new Error(`Response code: ${incomingMessage.statusCode}`)
          );
        }
        return resolve(html);
      }
    );
  });
};

class Net {
  static async GetHtmlPage(url) {
    let html;
    do {
      try {
        logger.debug(`Performing request (${url})...`);
        // eslint-disable-next-line no-await-in-loop
        html = await GetRequest(url);
        logger.debug(`...request (${url}) is finished`);
      } catch (e) {
        logger.error('(%s): %s', url, e);
        // eslint-disable-next-line no-await-in-loop
        await delay();
        logger.debug('Restart request (%s)', url);
      }
    } while (!html);
    return html;
  }
}

module.exports = Net;
