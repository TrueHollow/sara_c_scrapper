const cheerio = require('cheerio');
const Net = require('./Net');
const IO = require('./IO');
const db = require('../db');
const logger = require('../logger')('service/PersonsParser.js');

const SPOKEO_HOST = 'https://www.spokeo.com';

class PersonsParser {
  constructor(config) {
    this.config = config;
  }

  async getPersonData(url) {
    const personsURL = `${SPOKEO_HOST}${url}`;
    const html = await Net.GetHtmlPage(personsURL);
    const $ = cheerio.load(html);
    const divs = $('#phone_numbers div');
    let persons;
    divs.each((i, div) => {
      const reactBlock = div.attribs['data-react-class'];
      const props = div.attribs['data-react-props'];
      if (reactBlock === 'packs/v9/phone/containers/ExchangeListing' && props) {
        try {
          const data = JSON.parse(props);
          persons = data.people;
        } catch (e) {
          logger.error('JSON parsing (%s) err: %s', url, e);
        }
      }
    });
    if (!Array.isArray(persons)) {
      logger.warn('No persons, skipping saving (%s) (%s)', persons, url);
      persons = [];
    }
    return persons;
  }

  async run() {
    const { RequestsLimit } = this.config;
    const { Link, Sequelize } = db;
    const { Op } = Sequelize;
    const totalCount = await Link.count({
      where: { isStageFinished: true },
    });
    let counter = 0;
    let linksIteration;
    do {
      // eslint-disable-next-line no-await-in-loop
      linksIteration = await Link.findAll({
        where: { isStageFinished: true },
        limit: RequestsLimit,
      });
      // eslint-disable-next-line no-await-in-loop
      const personsIteration = await Promise.all(
        linksIteration.map(row => this.getPersonData(row.link))
      );
      // eslint-disable-next-line no-await-in-loop
      await Link.destroy({
        where: {
          id: {
            [Op.in]: linksIteration.map(row => row.id),
          },
        },
      });

      // eslint-disable-next-line no-await-in-loop
      const [left] = await Promise.all([
        Link.count({
          where: { isStageFinished: true },
        }),
        IO.SavePersonsCSV([].concat(...personsIteration)),
      ]);
      counter += RequestsLimit;
      logger.info(`Processed/All (${counter}/${totalCount}), left: ${left}`);
    } while (linksIteration && linksIteration.length);
  }
}

module.exports = PersonsParser;
