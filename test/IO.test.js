const chai = require('chai');
const IO = require('../service/IO');

const { expect } = chai;

describe('IO tests', () => {
  it('IO csv format testing.', async () => {
    const testCase = [
      {
        owners: ['Dan C******', 'Kristin C******'],
        phone_number: '(938) 432-1416',
        href: '/938-432-1416',
        details: [
          {
            value: 'General Service Carrier',
            title: 'carrier',
          },
          {
            value: null,
            title: 'type',
          },
          {
            value: 'Joseph City, AZ',
            title: 'location',
          },
        ],
      },
    ];
    await IO.SavePersonsCSV(testCase);
    await IO.SavePersonsCSV(testCase);
  });
});
